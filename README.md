# Wedoogift Frontend challenge
This repository is the result of the Wedoogift Frontend Challenge. You can find the rules here : https://gitlab.com/wedoogift-jobs/challenge/-/tree/master/frontend.

## Levels

There is 3 levels in this wedoogift challenge. This repository is meant to acheive the level 2.

## Run server side

First you need you run the server. To start it, go to calculator-server, run npm install and then npm start, the API will be listening on
port 3000.

## Run frontend application

You need to run the Angular application. To acheive that, you need to go to calculator-frontend, run npm install and then npm start. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.