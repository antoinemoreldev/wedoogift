import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'wedoogift-app';

  constructor(private translateService: TranslateService) {

  }

  ngOnInit() {
    this.translateService.setDefaultLang('fr');
    this.translateService.use('fr');
  }

  amountUpdate(amount: number) {
    console.log('Parent component is getting a new amount value : ', amount);
  }
}
