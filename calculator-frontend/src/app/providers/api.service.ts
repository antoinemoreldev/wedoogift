import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class APIService {
  private API_HOST: string = environment.backendUrl;
  private API_SHOP: string = '/shop';
  private API_SEARCH_COMBINATION = '/search-combination';

  constructor(private httpClient: HttpClient) {}

  getCombinationOfCards(shopId: number, amount: number): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'tokenTest123');
    const url = this.API_HOST + this.API_SHOP + '/' + shopId + this.API_SEARCH_COMBINATION;
    
    let params = new HttpParams();
    params = params.set('amount', String(amount));
    return this.httpClient.get(url, { headers, params});
  }

}
