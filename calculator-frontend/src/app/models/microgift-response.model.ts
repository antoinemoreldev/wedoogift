import { CombinationCards } from "./combination-cards.model";

export class MicroGiftResponse {
    public equal: CombinationCards;
    public floor: CombinationCards;
    public ceil: CombinationCards;

    constructor() {
    }
}
