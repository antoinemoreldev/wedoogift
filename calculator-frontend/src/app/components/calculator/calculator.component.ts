import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CombinationCards } from '../../models/combination-cards.model';
import { MicroGiftResponse } from '../../models/microgift-response.model';
import { APIService } from '../../providers/api.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  @Output() amountEvent = new EventEmitter<number>();

  loading: boolean = false;
  amountNotAvailable: boolean = false;
  amount: number;
  cards: number[] = [];
  floorCards: CombinationCards;
  ceilCards: CombinationCards;

  constructor(private apiService: APIService) { }

  ngOnInit() {
    this.initCards();
  }

  initCards() {
    this.cards = [];
    this.floorCards = null;
    this.ceilCards = null;
    this.amountNotAvailable = false;
  }

  validate() {
    this.loading = true;
    this.amountEvent.emit(this.amount);
    this.initCards();

    this.apiService.getCombinationOfCards(5, this.amount).subscribe(
      (response: MicroGiftResponse) => {
        this.handleRespone(response);
        this.loading = false;
      }
    );
  }

  handleRespone(response: MicroGiftResponse) {
    if(response.equal) {
      this.cards = response.equal.cards;
    } else {
      this.amountNotAvailable = true;
    }

    if(response.floor) {
      this.floorCards = response.floor;
    }

    if(response.ceil) {
      this.ceilCards = response.ceil;
    }
  }

  valideAmount(amount: CombinationCards) {
    this.amount = amount.value;
    this.validate();
  }

  getCeilAmount() {
    if(this.amount === this.ceilCards.value) {
      this.apiService.getCombinationOfCards(5, this.amount + 1).subscribe(
        (response: MicroGiftResponse) => {
          if(response.ceil) {
            this.amount = response.ceil.value;
            this.validate();
          } else {
            this.ceilCards = null;
          }
        }
      );
    } else {
      this.amount = this.ceilCards.value;
      this.validate();
    }
  }

  getFloorAmount() {
    if(this.amount === this.floorCards.value) {
      this.apiService.getCombinationOfCards(5, this.amount - 1).subscribe(
        (response: MicroGiftResponse) => {
          if(response.floor) {
            this.amount = response.floor.value;
            this.validate();
          } else {
            this.floorCards = null;
          }
        }
      );
    } else {
      this.amount = this.floorCards.value;
      this.validate();
    }
  }
}
